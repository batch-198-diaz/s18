// index.js

//Function able to receive data without the use of global variables or prompt();


//'name' - is a parameter
//A a parameter is a variable/container that exists only in our function and is used to store information that is provided to a function when it is called/invoked.

function printName (name) {
	console.log("My name is " + name);
}

// Data passed into a function invocation can be received by the function
//This is what we call an argument
printName("Juana");
printName("Jimin");

//Data passed into the function through function invocation is called arguments
//The argument is then stored within the container called a parameter

function printMyAge(age){
	console.log("I am " + age);
}

printMyAge(25);
printMyAge();
//console.log(age); //error

function check_divisibility_by_8(num){

	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleby8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleby8);
}

check_divisibility_by_8(64);
check_divisibility_by_8(27);

/*

	Mini-Activity A

	1. Create a function which is able to receive data as an argument. 
		- This function should be able to receive the name of your fave superhero.
		- Display the name of your favorite superhero in the console. 

	2. Debug the following function and invoke the method with an appropriate argument.

*/

// 1.

function print_input(superhero){
	console.log("My favourite superhero is " + superhero);
}

print_input("Doctor Strange")

// 2.

function printMyFavouriteLanguage(lang){
	console.log("My favourite language is: " + lang);
}

printMyFavouriteLanguage("Javascript");
printMyFavouriteLanguage("Java");


// MULTIPLE ARGUMENTS

// Multiple Arguments can also be passed into a function. 
//Multiple parameters can contain our arguments.

function printFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}

printFullName("Juan","Crisostomo","Ibarra");

/*
	Parameters will contain the arguments according to the order it was passed.

	Juan - firstName
	Crisostomo - middleName
	Ibarra - lastName

*/

// MORE/LESS ARGUMENTS THAN EXPECTED	

//In Javascript, providing less arguments than the expected params will automatically assign an undefined value to the parameter. In other languages, providing more/less arguments than expected parameters sometimes causes an error or changes the behaviour of the function. 

printFullName("Stephen","Wardell");					  //result: Stephen Wardell undefined
printFullName("Stephen","Wardell","Curry");		  //result: Stephen Wardell Curry
printFullName("Stephen","Wardell","Curry","Yes"); //result: Stephen Wardell Curry

//Using Variables as Arguments

let fName = "Larry";
let mName = "Joe";
let lName = "Bird";

printFullName(fName,mName,lName); //result: Larry Joe Bird

/*

	Mini-Activity B

	1. Create a function which whill be able to receive 5 arguments 
		- Receive 5 of your favourite songs
		- Display/Print the passed 5 fave songs in the console when func is invoked. 

*/

function print_fave_songs(fave_1,fave_2,fave_3,fave_4,fave_5){
	console.log("My favourite tracks are: ")
	console.log("1. " + fave_1);
	console.log("2. " + fave_2);
	console.log("3. " + fave_3);
	console.log("4. " + fave_4);
	console.log("5. " + fave_5);
}

print_fave_songs("Interlude", "August 10", "Pascal", "Hello I Love You", "Lost Game");



// RETURN STATEMENT

// So far, our functions are able to display data in our console. However, our functions cannot return values yet. Functions are able to return values which can be saved into a variable using the return statement/keyword.

let fullName = printFullName("Mark","Joseph","Lacdao");
console.log(fullName);

/*
result:

Mark Josepg Lacdao
undefined
*/

function returnFullName(firstName,middleName,lastName){

	return firstName + " " + middleName + " " + lastName;
}

fullName = returnFullName("Ernesto","Antonio","Maceda");
console.log(fullName);
console.log(fullName + " is my grandpa.");

/*
result:

Ernesto Antonio Maceda
Ernesto Antonio Maceda is my grandpa.
*/


//========================================================================================//

let variable_1 = console.log("yes"); //sample print a string and save it in a variable
console.log(variable_1); //it will print yes but now check the variable.. it is undefined


	function function_1(random_word){
		console.log(random_word);
	}

	variable_1 = function_1("yeehaw");	
	console.log("First result: ")
	console.log(variable_1);
//result is undefined, even though it prints it still doesn't give value to the variable


	function function_2(random_word){
		console.log(random_word);
		return random_word;
		console.log("I am after the return."); //would not show
	}

	let variable_2 = function_2("heehaw");
	console.log("Second Result: ");
	console.log(variable_2);
//because we added return statement.. it saves in the variable and ends the function.

//========================================================================================//

/*

	Mini-Activity C

	1. Debug the following function to be able to return a value and save it in a variable.

*/

function createPlayerInfo(username,level,job){

	//console.log("username: " + username, "level: " + level, "job: " + job);
	//return {username,level,job};

	return ("username: " + username + " level: " + level + " job: " + job);

}

let user1 = createPlayerInfo("whiteknight",95,"Paladin");
console.log(user1);