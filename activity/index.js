// index.js - activity


// 3. ADDITION

function addition (num_1,num_2) {
	let sum = num_1 + num_2;
	console.log("Displayed sum of " + num_1 + " and " + num_2);
	console.log(sum);
}

// 4. SUBTRACTION

function subtraction (num_1,num_2) {
	let diff = num_1 - num_2;
	console.log("Displayed difference of " + num_1 + " and " + num_2);
	console.log(diff);
}

// 5. 
addition(5,15);

// 6.
subtraction(20,5);


//================================================================================//


// 7. MULTIPLICATION

function multiplication (num_1,num_2) {
	console.log("The product of " + num_1 + " and " + num_2);
	let product_func = num_1 * num_2;
	return product_func;
}


//. 8 DIVISION

function division (num_1,num_2) {
	console.log("The quotient of " + num_1 + " and " + num_2);
	let quotient_func = num_1 / num_2;
	return quotient_func;
}


// 9.

let product = multiplication(50,10);
console.log(product);

// 10.

let quotient = division(50,10);
console.log(quotient);


//================================================================================//

// 11. AREA OF A CIRCLE

function area_of_circle(radius){
	console.log("The result of getting the area of a circle with " + radius + " radius:");
	let pi = 3.1416;
	let r_squared = Math.pow(radius,2);
	return pi * r_squared;
}

// 12.

let circleArea = area_of_circle(15);
console.log(circleArea);


//================================================================================//

// 13. AVERAGE

function average_num(num_1,num_2,num_3,num_4){
	console.log("The average of " + num_1 + ", " + num_2 + ", " + num_3 + " and " + num_4 + ":");
	let numbers = num_1 + num_2 + num_3 + num_4;
	return numbers / 4;
}

// 14. 

let averageVar = average_num(20,40,60,80);
console.log(averageVar);


//================================================================================//


// 15. PASSED GRADE

function grades(score,total){
	console.log("Is " + score + "/" + total + " a passing score?");
	let percentage = (score/total)*100
	let isPassed = percentage >= 75;
	return isPassed;
}

// 16.

let isPassingScore = grades(38,50);
console.log(isPassingScore);


//================================================================================//

